package io.c12.bala.sb.runner;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

@Component
@Slf4j
@Order(4)
@RequiredArgsConstructor
@Profile("test")
public class AseEncryptionRunner implements CommandLineRunner {

  @Override
  public void run(String... args) throws Exception {
    String password = "ch5mxdz3yza74pxwrbs5amhgpz9j3g3k5keyiixe2xdb224o96";
    String salt = "6aha6o6fq2tc3i5o4itm76js9";
    String aseAlgorithm = "AES/CBC/PKCS5Padding";       // AES/CBC/PKCS5Padding or AES/GCM/NoPadding

    SecretKey secretKey = getKeyFromPassword(password, salt);

    long startTime = System.nanoTime();
    IvParameterSpec iv = generateIv();

    String encStr = encrypt(aseAlgorithm, "pm_1LXq3nBhmn2nU6LTZzfh9RYN", secretKey, iv);
    String encIvStr = encStr + "." + Base64.getEncoder().encodeToString(iv.getIV());
    log.info("Encrypted string with iv - {}", encIvStr);
    log.info("End time - {} ns", (System.nanoTime() - startTime));
    var splitStr = encIvStr.split("\\.");
    var encData = splitStr[0];
    var encVi = splitStr[1];
    log.info("Decrypt String - {}", decrypt(aseAlgorithm, encData, secretKey, new IvParameterSpec(Base64.getDecoder().decode(encVi))));
    log.info("End time - {} ns", (System.nanoTime() - startTime));
  }

  private static SecretKey getKeyFromPassword(String password, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
    SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
    KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 65536, 256);
    return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
  }

  private static IvParameterSpec generateIv() {
    byte[] iv = new byte[16];
    new SecureRandom().nextBytes(iv);
    return new IvParameterSpec(iv);
  }

  private static String encrypt(String algorithm, String input, SecretKey key, IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
    Cipher cipher = Cipher.getInstance(algorithm);
    cipher.init(Cipher.ENCRYPT_MODE, key, iv);
    byte[] cipherText = cipher.doFinal(input.getBytes());
    return Base64.getEncoder().encodeToString(cipherText);
  }

  private static String decrypt(String algorithm, String cipherText, SecretKey key, IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
    Cipher cipher = Cipher.getInstance(algorithm);
    cipher.init(Cipher.DECRYPT_MODE, key, iv);
    byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));
    return new String(plainText);
  }
}
