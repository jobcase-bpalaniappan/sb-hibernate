package io.c12.bala.sb.runner;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import io.c12.bala.sb.db.model.CreditCard;
import io.c12.bala.sb.db.repository.CreditCardRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
@Order(3)
@RequiredArgsConstructor
@Profile("test")
public class TableEncryptionRunner implements CommandLineRunner {

  private final CreditCardRepository creditCardRepository;

  public static final char[] DEFAULT_ALPHABET =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

  @Override
  public void run(String... args) throws Exception {
    CreditCard creditCard = CreditCard.builder()
      .organizationId(UUID.randomUUID().toString())
      .profileId(NanoIdUtils.randomNanoId())
      .status("ACTIVE")
      .declineCount(0)
      .stripeCardIdEnc("pm_" + generateUniqueIds())
      .stripeCustomerIdEnc("cus_" + generateUniqueIds())
      .build();
    creditCardRepository.save(creditCard);

    var creditCardList = creditCardRepository.findAll();
    creditCardList.forEach(cc -> log.info("Credit card - {}", cc));
  }

  private static String generateUniqueIds() {
    return NanoIdUtils.randomNanoId(NanoIdUtils.DEFAULT_NUMBER_GENERATOR, DEFAULT_ALPHABET, 25);
  }
}
