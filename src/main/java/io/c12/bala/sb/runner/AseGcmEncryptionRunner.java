package io.c12.bala.sb.runner;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

@Component
@Slf4j
@Order(5)
@RequiredArgsConstructor
@Profile("test")
public class AseGcmEncryptionRunner implements CommandLineRunner {
  public static final int GCM_TAG_LENGTH = 16;

  public static final char[] DEFAULT_ALPHABET =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

  @Override
  public void run(String... args) throws Exception {
    String password = "ch5mxdz3yza74pxwrbs5amhgpz9j3g3k5keyiixe2xdb224o96";
    String salt = "6aha6o6fq2tc3i5o4itm76js9";

    // Generate Key
    SecretKey key = getKeyFromPassword(password, salt);

    for (int i = 0; i < 50; i++) {
      long startTime = System.nanoTime();
      IvParameterSpec iv = generateIv();

      String cipherText = encrypt("pm_" + generateUniqueIds(), key, iv.getIV());
      log.info("Encrypted Text : {}", cipherText);
      log.info("Time taken to encrypt {} ns", (System.nanoTime() - startTime));

      String cipherWithIv = cipherText + "." + Base64.getEncoder().encodeToString(iv.getIV());
      log.info("Cipher with IV - {}", cipherWithIv);
      var splitCipher = cipherWithIv.split("\\.");

      String decryptedText = decrypt(splitCipher[0], key, Base64.getDecoder().decode(splitCipher[1]));
      log.info("DeCrypted Text : {}", decryptedText);
      log.info("Time taken to encrypt and decrypt {} ns", (System.nanoTime() - startTime));
    }
  }

  public static String encrypt(String plaintext, SecretKey key, byte[] iv) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeyException {
    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
    GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
    cipher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec);
    return Base64.getEncoder().encodeToString(cipher.doFinal(plaintext.getBytes()));
  }

  public static String decrypt(String cipherText, SecretKey key, byte[] iv) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeyException {
    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
    GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
    cipher.init(Cipher.DECRYPT_MODE, key, gcmParameterSpec);
    return new String(cipher.doFinal(Base64.getDecoder().decode(cipherText)));
  }

  private static SecretKey getKeyFromPassword(String password, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
    SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
    KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 65536, 256);
    return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
  }

  private static IvParameterSpec generateIv() {
    byte[] iv = new byte[16];
    new SecureRandom().nextBytes(iv);
    return new IvParameterSpec(iv);
  }

  private static String generateUniqueIds() {
    return NanoIdUtils.randomNanoId(NanoIdUtils.DEFAULT_NUMBER_GENERATOR, DEFAULT_ALPHABET, 24);
  }
}
