package io.c12.bala.sb.runner;

import io.c12.bala.sb.db.model.OptCampaign;
import io.c12.bala.sb.db.repository.OptCampaignRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
@Order(1)
@RequiredArgsConstructor
@Profile("test")
public class HibernateJoinRunner implements CommandLineRunner {

  private final OptCampaignRepository optCampaignRepository;

  @Override
  public void run(String... args) throws Exception {
    List<OptCampaign> optCampaignList = optCampaignRepository.findAllByOrganizationId("3c0de336-f7fc-11ec-afea-23978bf11aaa");
    log.info("{}", optCampaignList);
  }
}
