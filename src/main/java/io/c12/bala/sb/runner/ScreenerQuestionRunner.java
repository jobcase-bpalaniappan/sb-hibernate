package io.c12.bala.sb.runner;

import io.c12.bala.sb.db.model.QuestionType;
import io.c12.bala.sb.db.model.ScreenerQuestion;
import io.c12.bala.sb.db.repository.ScreenerQuestionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.jcajce.provider.digest.Blake3;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@Slf4j
@Order(7)
@RequiredArgsConstructor
public class ScreenerQuestionRunner implements CommandLineRunner {

  private final ScreenerQuestionRepository screenerQuestionRepository;

  @Override
  public void run(String... args) throws Exception {
    final String question = "Do you have a valid dBjSYKCdoUT_6FSKrErGI to do the Job ?";
    final QuestionType questionType = QuestionType.SELECT;
    final String options = "[{\" label \": \" Yes \", \" value \": \" Yes \"}, {\" label \": \" No \", \"value \": \" No \"}]";
    final String uniqueId = createHashFromQuestionAndOption(question, questionType, options);

    final var screenerQuestion = ScreenerQuestion.builder().key(uniqueId).type(questionType).question(question).options(options).build();
    log.info("Screener Questions - {}", screenerQuestion);

    ExecutorService executorService = Executors.newCachedThreadPool();

    // Using executor service to simulate multiple inserts.
    for (int i = 0; i < 10; i++) {
      executorService.submit(() -> {
        ScreenerQuestion screenerQuestionResponse;
        try {
          screenerQuestionResponse = screenerQuestionRepository.saveAndFlush(screenerQuestion);
        } catch (DataIntegrityViolationException ex) {
          screenerQuestionResponse = screenerQuestionRepository.findFirstByKey(uniqueId);
        }
        log.info(" ----> Response {}", screenerQuestionResponse);
      });
    }
    executorService.shutdown();
  }

  private String createHashFromQuestionAndOption(String question, QuestionType questionType, String options) {
    String mergedString = (options != null) ? question.toLowerCase().replaceAll("\\s", "") + "~~" + questionType.name().toLowerCase() + "~~" + options.toLowerCase().replaceAll("\\s", "") : question.toLowerCase().replaceAll("\\s", "") + "~~" + questionType.name().toLowerCase();
    log.info("merged text - {}", mergedString);
    Blake3.Blake3_256 blake = new Blake3.Blake3_256();
    var response = blake.digest(mergedString.getBytes(StandardCharsets.UTF_8));
    return Base64.getUrlEncoder().withoutPadding().encodeToString(response);
  }

}
