package io.c12.bala.sb.runner;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import io.c12.bala.sb.dto.StudentDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
@Slf4j
@Order(2)
@RequiredArgsConstructor
@Profile("test")
public class SortRunner implements CommandLineRunner {

  @Override
  public void run(String... args) throws Exception {
    List<StudentDto> studentDtoList = new ArrayList<>();

    studentDtoList.add(StudentDto.builder().id(NanoIdUtils.randomNanoId()).name("Joe").birthDate(LocalDate.of(1990, 11, 3)).build());
    studentDtoList.add(StudentDto.builder().id(NanoIdUtils.randomNanoId()).name("James").birthDate(LocalDate.of(1984, 5, 26)).build());
    studentDtoList.add(StudentDto.builder().id(NanoIdUtils.randomNanoId()).name("Zoe").build());
    studentDtoList.add(StudentDto.builder().id(NanoIdUtils.randomNanoId()).birthDate(LocalDate.of(2001, 4, 15)).build());

    studentDtoList.sort(Comparator.comparing(StudentDto::getBirthDate, Comparator.nullsLast(Comparator.naturalOrder())));

    log.info("{}", studentDtoList);

    studentDtoList.sort(Comparator.comparing(StudentDto::getBirthDate, Comparator.nullsLast(Comparator.reverseOrder())));

    log.info("{}", studentDtoList);
  }
}
