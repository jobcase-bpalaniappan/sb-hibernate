package io.c12.bala.sb.runner;

import io.c12.bala.sb.db.repository.CreditCardRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Order(6)
@RequiredArgsConstructor
@Profile("test")
public class CustomJpaParamRunner implements CommandLineRunner {

  private final CreditCardRepository creditCardRepository;

  @Override
  public void run(String... args) throws Exception {
    log.info("Get Credit card partial info . . .");
    var response = creditCardRepository.getCustomCreditCardInfo("31a2c795-cfc0-41fd-96a4-28880088cf4b");
    log.info("Response -> {} | {} | {}", response.getOrganizationId(), response.getProfileId(), response.getStatus());
  }
}
