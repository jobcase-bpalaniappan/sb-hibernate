package io.c12.bala.sb.db.model;

import io.c12.bala.sb.db.converter.EncryptDecryptConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "credit_card")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class CreditCard {

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  @Column(name = "id", updatable = false, nullable = false, length = 36)
  private String key;

  @CreationTimestamp
  @Column(name = "created_at", nullable = false, updatable = false)
  private Date createdAt;

  @UpdateTimestamp
  @Column(name = "updated_at", nullable = false)
  private Date updatedAt;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "deleted_at")
  private Date deletedAt;

  @Column(name = "organization_id", length = 36)
  private String organizationId;

  @Column(name = "profile_id", length = 36)
  private String profileId;

  @Column(name = "status", length = 20)
  private String status = "ACTIVE";
  private Integer declineCount = 0;
  private Timestamp lastAttempt;

  // encrypt stripe card id
  @Convert(converter = EncryptDecryptConverter.class)
  private String stripeCardIdEnc;

  // encrypt stripe customer id
  @Convert(converter = EncryptDecryptConverter.class)
  private String stripeCustomerIdEnc;
}
