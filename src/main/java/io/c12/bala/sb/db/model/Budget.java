package io.c12.bala.sb.db.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "budget")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Budget implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  private Integer id;

  @Column(name = "jc_uuid", updatable = false, nullable = false, length = 36)
  private String jcUuid;

  @Column(name = "organization_id")
  private Integer organizationId;

  @Column(name = "advertiser_id")
  private Integer advertiserId;

  @Column(name = "campaign_id")
  private Integer campaignId;

  @Column(name = "start_date")
  private LocalDate startDate;

  @Column(name = "end_date")
  private LocalDate endDate;

  @Column(name = "budget")
  private Float budget;

  @Column(name = "status")
  private String status;

  @Column(name = "total_spend")
  private Float totalSpend;

  @Column(name = "pacing")
  private String pacing;

  @Column(name = "po_number")
  private String poNumber;

  @Column(name = "application_goal")
  private Integer applicationGoal;

}
