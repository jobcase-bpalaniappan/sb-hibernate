package io.c12.bala.sb.db.repository;

import io.c12.bala.sb.db.model.CreditCard;
import io.c12.bala.sb.dto.PartialCreditCard;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<CreditCard, String> {

  @Query("SELECT c.organizationId as organizationId, c.profileId as profileId, c.status as status FROM CreditCard c WHERE c.key = :id")
  PartialCreditCard getCustomCreditCardInfo(String id);
}
