package io.c12.bala.sb.db.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

@Entity
@Table(name = "optimization_campaign")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class OptCampaign implements Serializable {

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  @Column(name = "id", updatable = false, nullable = false, length = 36)
  private String key;

//    @Column(name = "campaign_id", updatable = false, nullable = false, length = 36)
//    private String campaignId;

  @Column(name = "external_url")
  private String externalUrl;

  @Column(name = "requisition_id")
  private String requisitionId;

  @Column(name = "creator_profile_id")
  private String creatorProfileId;

  @Column(name = "status")
  private String status;

  @Column(name = "organization_id")
  private String organizationId;

  @Column(name = "job_clicks")
  private Integer clicks;

  @Column(name = "site_id")
  private String siteId;

  @OneToOne(fetch = FetchType.EAGER, targetEntity = Campaign.class)
  @Fetch(FetchMode.JOIN)
  @JoinColumn(name = "campaign_id", updatable = false, nullable = false, referencedColumnName = "jc_uuid")
  private Campaign campaign;
}
