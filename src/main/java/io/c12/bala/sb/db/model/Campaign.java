package io.c12.bala.sb.db.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "campaign")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Campaign implements Serializable {

  @Id
  @Column(name = "id", updatable = false, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "jc_uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String jcUuid;

  @Column(name = "name")
  private String name;

  @Column(name = "campaign_description")
  private String campaignDescription;

  @Column(name = "advertiser_id")
  private Integer advertiserId;

  @Column(name = "approved")
  private String published;

  @OneToMany(fetch = FetchType.EAGER, targetEntity = Budget.class)
  @Fetch(FetchMode.JOIN)
  @JoinColumn(name = "campaign_id")
  private Set<Budget> budgets;
}
