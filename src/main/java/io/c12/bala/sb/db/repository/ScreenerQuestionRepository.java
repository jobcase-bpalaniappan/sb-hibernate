package io.c12.bala.sb.db.repository;

import io.c12.bala.sb.db.model.ScreenerQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScreenerQuestionRepository extends JpaRepository<ScreenerQuestion, String> {

  ScreenerQuestion findFirstByKey(String key);
}
