package io.c12.bala.sb.db.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

@Slf4j
@Converter
public class EncryptDecryptConverter implements AttributeConverter<String, String> {

  private static final String AES_SECRET = "ch5mxdz3yza74pxwrbs5amhgpz9j3g3k5keyiixe2xdb224o96";
  private static final String SALT = "6aha6o6fq2tc3i5o4itm76js9";
  private static final String ENCRYPTION_ALGORITHM = "AES/CBC/PKCS5Padding";

  @Override
  public String convertToDatabaseColumn(String attribute) {
    try {
      final SecretKey secretKey = getKeyFromPassword(AES_SECRET, SALT);
      final IvParameterSpec iv = generateIv();
      String encStr = encrypt(ENCRYPTION_ALGORITHM, attribute, secretKey, iv);
      return encStr + "." + Base64.getEncoder().encodeToString(iv.getIV());
    } catch (Exception e) {
      log.error("Error ", e);
    }
    return null;
  }

  @Override
  public String convertToEntityAttribute(String dbData) {
    try {
      SecretKey secretKey = getKeyFromPassword(AES_SECRET, SALT);
      var splitStr = dbData.split("\\.");
      var encData = splitStr[0];
      var encVi = splitStr[1];
      return decrypt(ENCRYPTION_ALGORITHM, encData, secretKey, new IvParameterSpec(Base64.getDecoder().decode(encVi)));
    } catch (Exception e) {
      log.error("Error ", e);
    }
    return null;
  }

  private static SecretKey getKeyFromPassword(String password, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
    SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
    KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 65536, 256);
    return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
  }

  private static IvParameterSpec generateIv() {
    byte[] iv = new byte[16];
    new SecureRandom().nextBytes(iv);
    return new IvParameterSpec(iv);
  }

  private static String encrypt(String algorithm, String input, SecretKey key, IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
    Cipher cipher = Cipher.getInstance(algorithm);
    cipher.init(Cipher.ENCRYPT_MODE, key, iv);
    byte[] cipherText = cipher.doFinal(input.getBytes());
    return Base64.getEncoder().encodeToString(cipherText);
  }

  private static String decrypt(String algorithm, String cipherText, SecretKey key, IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
    Cipher cipher = Cipher.getInstance(algorithm);
    cipher.init(Cipher.DECRYPT_MODE, key, iv);
    byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));
    return new String(plainText);
  }

}
