package io.c12.bala.sb.db.repository;

import io.c12.bala.sb.db.model.OptCampaign;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OptCampaignRepository extends CrudRepository<OptCampaign, String> {

  List<OptCampaign> findAllByOrganizationId(String organizationId);
}
