package io.c12.bala.sb.db.model;

public enum QuestionType {
  SELECT,
  MULTISELECT,
  TEXT,
  TEXTAREA,
  DATE,
  NUMBER
}
