package io.c12.bala.sb.dto;

public interface PartialCreditCard {
  String getOrganizationId();

  String getProfileId();

  String getStatus();
}
