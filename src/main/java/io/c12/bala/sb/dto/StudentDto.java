package io.c12.bala.sb.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class StudentDto {
  private String id;
  private String name;
  private LocalDate birthDate;
}
