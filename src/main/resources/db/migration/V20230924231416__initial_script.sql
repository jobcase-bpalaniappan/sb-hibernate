CREATE SCHEMA IF NOT EXISTS `test`;

CREATE TABLE `budget`
(
  id               INT AUTO_INCREMENT NOT NULL,
  jc_uuid          VARCHAR(36)        NOT NULL,
  organization_id  INT                NULL,
  advertiser_id    INT                NULL,
  campaign_id      INT                NULL,
  start_date       date               NULL,
  end_date         date               NULL,
  budget           FLOAT              NULL,
  status           VARCHAR(255)       NULL,
  total_spend      FLOAT              NULL,
  pacing           VARCHAR(255)       NULL,
  po_number        VARCHAR(255)       NULL,
  application_goal INT                NULL,
  CONSTRAINT pk_budget PRIMARY KEY (id)
);

CREATE TABLE `campaign`
(
  id                   INT AUTO_INCREMENT NOT NULL,
  jc_uuid              VARCHAR(255)       NULL,
  name                 VARCHAR(255)       NULL,
  campaign_description VARCHAR(255)       NULL,
  advertiser_id        INT                NULL,
  approved             VARCHAR(255)       NULL,
  CONSTRAINT pk_campaign PRIMARY KEY (id)
);

CREATE TABLE `credit_card`
(
  `id`                     VARCHAR(36)  NOT NULL,
  `created_at`             datetime     NOT NULL,
  `updated_at`             datetime     NOT NULL,
  `deleted_at`             datetime     NULL,
  `organization_id`        VARCHAR(36)  NULL,
  `profile_id`             VARCHAR(36)  NULL,
  `status`                 VARCHAR(20)  NULL,
  `decline_count`          INT          NULL,
  `last_attempt`           datetime     NULL,
  `stripe_card_id_enc`     VARCHAR(255) NULL,
  `stripe_customer_id_enc` VARCHAR(255) NULL,
  CONSTRAINT pk_credit_card PRIMARY KEY (`id`)
);

CREATE TABLE `optimization_campaign`
(
  `id`                 VARCHAR(36)  NOT NULL,
  `external_url`       VARCHAR(255) NULL,
  `requisition_id`     VARCHAR(255) NULL,
  `creator_profile_id` VARCHAR(255) NULL,
  `status`             VARCHAR(255) NULL,
  `organization_id`    VARCHAR(255) NULL,
  `job_clicks`         INT          NULL,
  `site_id`            VARCHAR(255) NULL,
  `campaign_id`        VARCHAR(36)  NOT NULL,
  CONSTRAINT pk_optimization_campaign PRIMARY KEY (`id`)
);

ALTER TABLE `budget`
  ADD CONSTRAINT FK_BUDGET_ON_CAMPAIGN FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`);
