CREATE TABLE `screener_question`
(
  `id`         bigint                                                               not null auto_increment primary key,
  `hash_key`   char(45)                                                             not null,
  `question`   text                                                                 null,
  `type`       enum ('SELECT', 'MULTISELECT', 'TEXT', 'TEXTAREA', 'DATE', 'NUMBER') not null,
  `options`    json                                                                 null,
  `created_at` datetime default CURRENT_TIMESTAMP                                   not null,
  `updated_at` datetime default CURRENT_TIMESTAMP                                   not null,
  `deleted_at` datetime                                                             null
) collate = utf8mb4_bin;

ALTER TABLE `screener_question`
  ADD INDEX idx_deleted_at (`deleted_at`);

ALTER TABLE `screener_question`
  ADD UNIQUE INDEX `uniq_idx_key` (`hash_key`);
